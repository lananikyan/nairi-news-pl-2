"use strict";


jQuery(document).ready(function ($) {
    function refreshLeft() {
        $("#main_menu").removeAttr("data-expanded");
        $("#header-dropdown").removeClass("active");
    }
    
    $(window).load(function () {
        $(".loaded").fadeOut();
        $(".preloader").delay(1000).fadeOut("slow");
        refreshLeft();
    });

    $('#navbar-collapse').find('a[href*=#]:not([href=#])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: (target.offset().top - 40)
                }, 1000);
                if ($('.navbar-toggle').css('display') != 'none') {
                    $(this).parents('.container').find(".navbar-toggle").trigger("click");
                }
                return false;
            }
        }
    });

    $(".mod-header .primary").on('mouseenter',".has-children", function(){
        $(this).find("[data-level='2']").addClass("open");
    });
    $(".mod-header .primary").on('mouseleave', ".has-children", function(){
        $(this).find("[data-level='2']").removeClass("open");
        $(this).find("[data-level='3']").removeClass("open");
        $(this).find(".has-child-group .toggle").removeClass("expanded");
    });
    $(".mod-header .primary").on("click", ".has-child-group .toggle", function(){
        if($(this).hasClass("expanded"))
        {
            $(this).removeClass("expanded");
            $(this).parent().find("ul").removeClass("open");
        }
        else {
            $(this).addClass("expanded");
            $(this).parent().find("ul").addClass("open");
        }
    });
    $("#hamburger").click(function(){
        if($("#main_menu").attr("data-expanded") != undefined)
        {
            $("#main_menu").removeAttr("data-expanded");
            $("#header-dropdown").removeClass("active");
            $("#main_body").removeClass("no-scroll");
        }
        else {
            $("#main_menu").attr("data-expanded", false);
            $("#header-dropdown").addClass("active");
            $("#main_body").addClass("no-scroll");
        }
    });
    $("#header-dropdown .primary").on("click", ".has-children .toggle", function(){
        if($(this).parent().attr("data-children-visible") != undefined)
        {
            $(this).parent().removeAttr("data-children-visible");
            $(this).removeClass("expanded");

        }
        else {
            $(this).parent().attr("data-children-visible", true);
            $(this).addClass("expanded");
        }
    });
    $( window ).resize(function() {
        var width = $( window ).width();
        
        if(width >= 1024 )
        {
            $("#main_menu").removeAttr("data-expanded");
            $("#header-dropdown").removeClass("active");
        }
    });
});
